import { createHeaders } from "./index"

const apiUrl = process.env.REACT_APP_API_URL

// Check if user already exist in API.
const checkForUser = async (username) => {
  try {
    const response = await fetch(`${apiUrl}?username=${username}`)
    if (!response.ok) {
      throw new Error("Could not complete request")
    }
    const data = await response.json()
    return [null, data]
  } catch (error) {
    return [error.message, []]
  }
}

// Add new user to API.
const createUser = async (username) => {
  try {
    const response = await fetch(apiUrl, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        username,
        translations: [],
      }),
    })
    if (!response.ok) {
      throw new Error("Could not create user" + username)
    }
    const data = await response.json()
    return [null, data]
  } catch (error) {
    return [error.message, []]
  }
}

//first called -> checks for existing users -> creates new user if none is found.
export const loginUser = async (username) => {
  const [checkError, user] = await checkForUser(username)

  if (checkError !== null) {
    return [checkError, null]
  }

  if (user.length > 0) {
    return [null, user.pop()]
  }

  return await createUser(username)
}

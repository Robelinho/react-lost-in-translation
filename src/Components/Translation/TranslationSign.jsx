const TranslationSign = ({ character, index }) => {
  // Construct path to correct image.
  const imagePath = "images/" + character.toLowerCase() + ".png"

  if (character === " ") {
    return <p className="hand-sign"></p>
  }

  return (
    <img key={index} src={imagePath} alt="Hand gesture" className="hand-sign" />
  )
}
export default TranslationSign

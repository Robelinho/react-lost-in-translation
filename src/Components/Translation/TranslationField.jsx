import TranslationSign from "./TranslationSign"

const TranslationField = ({ input }) => {
  let inputArr = []
  let translationResult = ""

  // If input is not empty -> convert input to signs.
  if (input) {
    inputArr = input.split("")
    translationResult = inputArr.map((gesture, index) => {
      return <TranslationSign character={gesture} key={index} />
    })
  }

  return (
    <div id="translation-field">
      <div id="translation-result">{translationResult}</div>
      <div id="translation-field-bottom-bar">
        <p id="translation-tag">Translation</p>
      </div>
    </div>
  )
}
export default TranslationField

import ProfileTranslationsHistoryItem from "./ProfileTranslationsHistoryItem"

const ProfileTranslationsHistory = ({ translations }) => {
  if (translations) {
    // Sort translations into correct order, based on if its more than 10 items or not.
    // If 10 or more, it only selects the last 10 items.
    translations.length >= 10
      ? (translations = translations
          .slice(translations.length - 10, translations.length)
          .reverse())
      : (translations = translations.slice(0, translations.length).reverse())
  }

  const translationList = translations.map((translation, index) => {
    return (
      <ProfileTranslationsHistoryItem
        key={index + "-" + translation}
        translation={translation}
      />
    )
  })

  return (
    <section id="translation-list">
      <h4 id="translation-history-headline">Recent Translations</h4>
      <ul id="profile-history-list">{translationList}</ul>
    </section>
  )
}
export default ProfileTranslationsHistory

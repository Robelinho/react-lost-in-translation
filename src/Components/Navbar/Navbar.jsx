import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"

const Navbar = () => {
  const { user } = useUser()

  return (
    <nav id="navbar">
      <h1 id="navbar-header">Lost in Translation</h1>
      <div id="nav-separator"></div>
      {user !== null && (
        <ul id="navbar-list">
          <li className="navbar-action">
            <NavLink to="/translate">Translate</NavLink>
          </li>
          <li className="navbar-action" id="nav-list-end">
            <NavLink id="profile-nav-item" to="/profile">
              <img
                id="profile-nav-avatar"
                src="images/profile-avatar.png"
                alt="Profile Avatar"
              />
              <p id="navbar-profile-text">Profile</p>
            </NavLink>
          </li>
        </ul>
      )}
    </nav>
  )
}
export default Navbar

import { useState } from "react"
import TranslationActions from "../Components/Translation/TranslationActions"
import TranslationField from "../Components/Translation/TranslationField"
import withAuth from "../hoc/withAuth"

const Translator = () => {
  const [input, setInput] = useState()

  return (
    <div id="translation-window">
      <h1>Translator</h1>
      <TranslationActions setInput={setInput} />
      <TranslationField input={input} />
    </div>
  )
}
export default withAuth(Translator)

# Getting Started with Create React App

### Contributors
Marcus Jarlevid & Oliver Rimmi

### NOTE
We decided to store data in sessionStorage to prevent data mismatch between application and API.

### Usage
To get this project working you will have to create your own .env file.
1. Add a file named `.env` in the `src` folder.
2. Add `REACT_APP_API_KEY=` on line 1.
3. Add `REACT_APP_API_URL=` on line 2.
4. KEY and URL will be provided. 
5. Add provided keys to corresponding variable. Ex: `REACT_APP_API_KEY=thisIsAKey`.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.
